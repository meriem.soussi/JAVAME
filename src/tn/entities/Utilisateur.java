/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.entities;

import java.util.Date;
import java.util.Random;

/**
 *
 * @author Iserv
 */
public class Utilisateur {
    
    private int id;
    private int idFormation;
    private String username;
    private String usernameCanonical=username;
    private String email;
    private String emailCanonical=email;
    private int enabled=1;
    private String password;
        private String salt=password;

    private Date lastLogin=null;
    private int locked=1;
    private int expired=0;
    private Date expiresAt=null;
    private String confirmationToken=null;
    private Date passwordRequestedAt=null;
    private String roles;
    private int credentialsExpired=0;
    private Date credentialsExpiredAt=null;
    private String nom;
    private String prenom;
    private int cin;
    private String adresse;
    private int telephone;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    private String grade;
    private int etat;
     private String description;
         private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFormation() {
        return idFormation;
    }

    public void setIdFormation(int idFormation) {
        this.idFormation = idFormation;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameCanonical() {
        return usernameCanonical;
    }

    public void setUsernameCanonical(String usernameCanonical) {
        this.usernameCanonical = usernameCanonical;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailCanonical() {
        return emailCanonical;
    }

    public void setEmailCanonical(String emailCanonical) {
        this.emailCanonical = emailCanonical;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public int getLocked() {
        return locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public Date getPasswordRequestedAt() {
        return passwordRequestedAt;
    }

    public void setPasswordRequestedAt(Date passwordRequestedAt) {
        this.passwordRequestedAt = passwordRequestedAt;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public int getCredentialsExpired() {
        return credentialsExpired;
    }

    public void setCredentialsExpired(int credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
    }

    public Date getCredentialsExpiredAt() {
        return credentialsExpiredAt;
    }

    public void setCredentialsExpiredAt(Date credentialsExpiredAt) {
        this.credentialsExpiredAt = credentialsExpiredAt;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getCin() {
        return cin;
    }

    public void setCin(int cin) {
        this.cin = cin;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

   
    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
 
    public Utilisateur() {
        this.password=password;
        this.username=username;
        this.usernameCanonical=username;
        this.email=email;
        this.emailCanonical=email;
        this.enabled = enabled;
        
        Random rn = new Random();

        this.locked = 0;
        this.expired = 0;
        this.roles = roles;
        this.credentialsExpired = 0;
        //this.roles="sannou3";
        this.credentialsExpired=0;
    }

    public Utilisateur(int id, String username, String usernameCanonical, String email, String emailCanonical, int enabled, String salt, String password, Date lastLogin, int locked, int expired, Date expiresAt, String confirmationToken, Date passwordRequestedAt, String roles, int credentialsExpired, Date credentialsExpiredAt) {
        this.id = id;
        this.username = username;
        this.usernameCanonical = usernameCanonical;
        this.email = email;
        this.emailCanonical = emailCanonical;
        this.enabled = enabled;
        this.salt = salt;
        this.password = password;
        this.lastLogin = lastLogin;
        this.locked = locked;
        this.expired = expired;
        this.expiresAt = expiresAt;
        this.confirmationToken = confirmationToken;
        this.passwordRequestedAt = passwordRequestedAt;
        this.roles = roles;
        this.credentialsExpired = credentialsExpired;
        this.credentialsExpiredAt = credentialsExpiredAt;
    }

    public Utilisateur(int id, String username, String usernameCanonical, String email, String emailCanonical, int enabled) {
        this.id = id;
        this.username = username;
        this.usernameCanonical = usernameCanonical;
        this.email = email;
        this.emailCanonical = emailCanonical;
        this.enabled = enabled;
    }

     public Utilisateur( String username, String email, int enabled, String roles ,int id) {
    this.username = "username";
    this.email = "email";
    this.enabled = 1;
    this.roles = "roles";
        this.id = 1;

    }

    public Utilisateur(int id,int idFormation ,String username, String email, String password, String roles, String nom,String prenom ,int cin, String adresse ,int telephone ,String grade, int etat ,String description ,String image) {
        this.id = id;
        this.idFormation=idFormation;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.nom=nom;
       this.prenom=prenom;
       this.cin=cin;
       this.adresse=adresse;
       this.telephone=telephone;
       this.grade=grade;
       this.etat=etat;
       this.description=description;
      this.image = image;
    }
        
       
       
     
    public Utilisateur(int id) {
        this.id = id;
    }

    public Utilisateur(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public Utilisateur(String username) {
        this.username = username;
    }

    public String toString() {
        return "User{" + "id=" + id +  ", idFormation=" + idFormation + ", username=" + username + ", usernameCanonical=" + usernameCanonical + ", email=" + email + ", emailCanonical=" + emailCanonical + ", enabled=" + enabled + ", password=" + password + ", salt=" + salt + ", lastLogin=" + lastLogin + ", locked=" + locked + ", expired=" + expired + ", expiresAt=" + expiresAt + ", confirmationToken=" + confirmationToken + ", passwordRequestedAt=" + passwordRequestedAt + ", roles=" + roles + ", credentialsExpired=" + credentialsExpired + ", credentialsExpiredAt=" + credentialsExpiredAt +  ", nom=" + nom + ", prenom=" + prenom +", cin=" + cin +  ", adresse=" + adresse + ", telephone=" + telephone +" , grade=" + grade +", etat=" + etat +", description=" + description +", image=" + image +'}';
    }

    
    
   
    

  
}


 
