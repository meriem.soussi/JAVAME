/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.gui;

import tn.entities.User;
import handlers.UserHandler;
import java.io.DataInputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 *
 * @author Ch3oul
 */
public class rechercherUser implements CommandListener, Runnable{
    Display display;
    Command cmdParse = new Command("rechercher", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.BACK, 0);
    Command cmdBack1 = new Command("Back", Command.BACK, 0);
        Command cmdBack2 = new Command("Back", Command.BACK, 0);

    User[] users;
    User user;
    List lst = new List("Users", List.IMPLICIT);
    Form f = new Form("Accueil");
    TextField tfNom = new TextField("nom", null, 100, TextField.ANY);
    Form form = new Form("User");
    Form loadingDialog = new Form("Please Wait");
    StringBuffer sb = new StringBuffer();
    
    int ch;

    public rechercherUser(Display display,User user) {

        
        this.display = display;
        this.user = user;
        f.append("Rechercher utilisateur");
        f.addCommand(cmdParse);
        f.addCommand(cmdBack);
        f.append(tfNom);
        f.setCommandListener(this);
        lst.setCommandListener(this);
        lst.addCommand(cmdBack1);
        form.addCommand(cmdBack2);
        form.setCommandListener(this);
        
        display.setCurrent(f);
        
    }
    
    public void commandAction(Command c, Displayable d) {

        if (c == cmdParse) {
            display.setCurrent(loadingDialog);
            Thread th = new Thread(this);
            th.start();
        }

        if (c == List.SELECT_COMMAND) {
            form.append("Informations User: \n");
            form.append(showPersonne(lst.getSelectedIndex()));
            display.setCurrent(form);
        }
        if (c == cmdBack1) {
            display.setCurrent(f);
        }
          if (c == cmdBack2) {
            display.setCurrent(f);
        }
        if (c == cmdBack) {
            new Acceuil(display,user);
        }

    }

    public void run() {
        try {
            // this will handle our XML
            UserHandler userssHandler = new UserHandler();
            // get a parser object
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            //System.out.println(url +"?username_canonical="+tfNom.getString().trim());
            System.out.println(tfNom.getString());
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/parsing2/rechercher.php?username="+tfNom.getString());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            System.out.println("2");
            parser.parse(dis, userssHandler);
            System.out.println("3");
            // display the result
            users = userssHandler.getUser();
            System.out.println(users.length);

            if (users.length > 0) {
                for (int i = 0; i < users.length; i++) {
                    lst.append(users[i].getUsername()+" "
                            +users[i].getEmail(), null);

                }
            }

        } catch (Exception e) {
            System.out.println("Exception:" + e.toString());
        }
        display.setCurrent(lst);
    }

    private String showPersonne(int i) {
        String res = "";
        if (users.length > 0) {
            sb.append("* ");
            sb.append(users[i].getUsername());
            sb.append("\n");
            sb.append("* ");
            sb.append(users[i].getEmail());
            sb.append("\n");
        }
        res = sb.toString();
        sb = new StringBuffer("");
        return res;
    }
}

