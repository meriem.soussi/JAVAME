package tn.gui;


import java.io.DataInputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class ConsulterMatchh implements CommandListener {


    private User utilisateur;
    private Display display;
    private Match[] matchs;
    String labels[] = {"", "", "", "", "", "", ""};
    Command cmdBackMenu = new Command("Retour", Command.BACK, 0);
    Command cmdAjouterMatch = new Command("Ajouter", Command.OK, 0);
    Image[] img = new Image[5];

    public ConsulterCatalogue(Display display, User utilisateur) {

        selector = new iPhoneCanvas("liste des Matchs", List.IMPLICIT,labels,img);
        this.display = display;
        this.utilisateur = utilisateur;
        selector.addCommand(cmdBackMenu);
        selector.addCommand(cmdAjouterMatch);
        selector.setCommandListener(this);
        getMatchsHttp();
        display.setCurrent(selector);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBackMenu) {
            new Acceuil(display, utilisateur);
        }
    if (c == cmdAjouterCatalogue) {
          new ajouterMatch(display, utilisateur);
        }
       if (c == List.SELECT_COMMAND) {
            for(int j=0 ;j<matchs.length;j++)
            {System.out.println(Matchs[j]);}
            System.out.println("list" + selector.getSelectedIndex() + " " + Matchs.length);
            int a = (int) (selector.getSelectedIndex()+3) %Matchs.length;
            if (a < 0) {
                a = Matchs.length + a;
            }
            System.out.println("indice" + a);
            ConsulterDetailMatch consulterDetailMatch = new ConsulterDetailMatch(display, Matchs[a], utilisateur);
        }
    }

    private void getMatchsHttp() {
        Thread th = new Thread() {
            public void run() {
                selector.deleteAll();
                catalogues = null;
                try {
                    CatalogueHandler catalogueHandler = new CatalogueHandler();
                    // get a parser object
                  
                    SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                    // get an InputStream from somewhere (could be HttpConnection, for example)
                    HttpConnection hc = (HttpConnection) Connector.open("http://localhost/parsing2/getXmlCatalogueAttributes.php");//people.xml est un exemple
                    DataInputStream dis = new DataInputStream(hc.openDataInputStream());
                   
                    parser.parse(dis, catalogueHandler);
                 
                    // display the result
                    
                    Matchs = catalogueHandler.getTabCatalogues();
                    img = new Image[Matchs.length];
                 
                    if (Matchs.length > 0) {  ;
                        for (int i = 0; i < Matchs.length; i++) { 
                            System.out.println(Matchs[i].getDescription());
                            labels[i] = Matchs[i].getDescription()+" Nom: "+Matchs[i].getNom();
                           
                                img[i] = getImage(Match[i].getImage());
                                  System.out.println("fer");
                               //Image im = Image.createImage("/images/thumb8.jpg");
                               //img[i] = im;
                                System.out.println(" label "+labels[i]);
                           
                                selector.append(labels[i], img[i]);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Exception:" + e.toString());
                }
                System.out.println(Matchs.length);
                display.setCurrent(selector);
            }
        };
        th.start();
    }

    public Image getImage(String url) {
        DataInputStream is = null;

        Image img = null;
        try {
            HttpConnection c = (HttpConnection) Connector.open(url);
            int len = (int) c.getLength();
            if (len > 0) {
                is = c.openDataInputStream();
                byte[] data = new byte[len];
                is.readFully(data);
                img = Image.createImage(data, 0, len);
                return img;
            }
            is.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
        return null;
    }
}
