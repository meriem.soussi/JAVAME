/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.gui;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import javax.microedition.lcdui.Ticker;

/**
 *
 * @author Iserv
 */
public class AjouterMatch implements CommandListener {
    
    private Form formulaireAjoutCatalogue = new Form("Ajouter Match");
    private Ticker tk = new Ticker("Ajouter Match");
    private String[] typeItems = {"specifique", "general"};
    private ChoiceGroup type = new ChoiceGroup("select one type", ChoiceGroup.POPUP, typeItems, null);
    
    private TextField type = new TextField("type", null, 40, TextField.ANY);

    
    private Command cmdValider = new Command("Valider", Command.SCREEN, 0);
    private Command cmdBack = new Command("Back", Command.EXIT, 0);
    private User utilisateur;
    private Display display;

    public AjouterCatalogue(Display display, User utilisateur) {
        this.display = display;
        this.utilisateur = utilisateur;
        
        formulaireAjoutMatch.setTicker(tk);
        formulaireAjoutMatch.append(type);
        
        
        formulaireAjoutMatch.append(type);
                formulaireAjoutMatch.append(lieux);

                
        formulaireAjoutMatch.append(imageUrl);
        formulaireAjoutMatch.addCommand(cmdValider);
        formulaireAjoutMatch.addCommand(cmdBack);
        formulaireAjoutMatch.setCommandListener(this);
        display.setCurrent(formulaireAjoutMatch);
        
    }
    public void commandAction(Command c, Displayable d) {
        if(c==cmdValider){
            ajoutCatalogueHttp();
        }else if(c==cmdBack){
            new ConsulterCatalogue(display, utilisateur);
        }
    }
    
   
    
        public void ajoutMatchHttp() {
        Thread th = new Thread() {
            public void run() {
                try { 
                    String url = "http://localhost/parsing2/ajoutMatch.php?";
                    HttpConnection hc;
                    DataInputStream dis;
                    StringBuffer sb = new StringBuffer();
                    System.out.println(type.getSelectedIndex());
                    String typeString = type.getString(type.getSelectedIndex());
                    String datedebutString = datedebut.getString();
                    String lieuxString = lieux.getString();
                    
                    

                    int ch;
                    Alert alerta = new Alert(null, null, null, AlertType.ERROR);
                    hc = (HttpConnection) Connector.open(url 
                            + "type=" + typeString
                            +"&lieux="+ nomString
                             + "&Datedebut=" + descriptionString);
                   
                    // ?nom=a+prenom=a+pseudo=a+email=a+passe=a+adresse=a+telephone=123+role=client
                    dis = new DataInputStream(hc.openDataInputStream());
                    while ((ch = dis.read()) != -1) {
                        sb.append((char) ch);
                    }
                    if ("successfully added".equalsIgnoreCase(sb.toString().trim())) { 
                     //   adresse.setString("");
                        type.setSelectedIndex(0, true);
                       // latitude.setString("");
                        
                        tupe.setString("");
                        alerta.setTitle("Excellent travail");
                        alerta.setString("Catalogue ajouté avec succes\najoutez un autre si vous voulez");
                        alerta.setTimeout(4000);
                        alerta.setType(AlertType.INFO);

                        display.setCurrent(alerta);
                    } else {
                        alerta.setTitle("Erreur");
                        alerta.setString("désolé, ajout échoué");
                        alerta.setType(AlertType.ERROR);
                        alerta.setTimeout(2000);
                        display.setCurrent(alerta);
                          new ConsulterCatalogue(display, utilisateur);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        };
        th.start();
    }
    
}
   
}
