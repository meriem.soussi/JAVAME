package tn.gui;



import handlers.UserHandler;
import tn.entities.User;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Gauge;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.Spacer;
import javax.microedition.lcdui.TextField;
import javax.microedition.lcdui.Ticker;

public class Profil implements CommandListener {

    private Form form = new Form("Infos Client");
    private Form formlst = new Form("Favoris");
    private Form loadingDialog = new Form("Please Wait");
    private Display disp;
    private Ticker tk;
    Image img;
    private User utilisateur;
    private StringBuffer sb = new StringBuffer();
    private Command cmdBack = new Command("Back", Command.BACK, 0);
 
    private Command cmdModifierMonProfil = new Command("ModifierProfil", Command.SCREEN, 0);
    Gauge gau = new Gauge(null, false, Gauge.INDEFINITE, Gauge.CONTINUOUS_RUNNING);
     private Command cmdsupuser = new Command("Désactiver compte ", Command.BACK, 0);

    Form modif = new Form("Modifier");
    Command cmdvalidermodifprofil = new Command("valider", Command.SCREEN, 0);
     Command cmdd = new Command("map", Command.SCREEN, 0);
    TextField newnom = new TextField("Nom  :", null, 30, TextField.ANY);
    TextField newmail = new TextField("Email :", null, 30, TextField.ANY);
    TextField newpassword = new TextField("Mot de passe :", null, 30, TextField.ANY);
    Alert alerta = new Alert(null, null, null, AlertType.ERROR);
    int ch;
   

    public Profil(Display disp, User utilisateur) {
        this.disp = disp;
        this.utilisateur = utilisateur;
        tk = new Ticker("Salut " + utilisateur.getUsername());
        form.setTicker(tk);
        form.setCommandListener(this);
        form.addCommand(cmdBack);   
        form.addCommand(cmdModifierMonProfil);
        form.addCommand(cmdsupuser);

        form.addCommand(cmdd);
        form.setCommandListener(this);
        loadingDialog.append(new Spacer(100, 100));
        gau.setLayout(Item.LAYOUT_CENTER);
        loadingDialog.append(gau);

        form.append("Informations Client: \n");
        form.append(showUtilisateur());
        modif.setTicker(tk);
        modif.addCommand(cmdvalidermodifprofil);
        modif.setCommandListener(this);
       
        disp.setCurrent(form);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            
            new Acceuil(disp,utilisateur);
        }
        if (c == cmdModifierMonProfil) {
            updateProfil(utilisateur);
        }
        if (c == cmdvalidermodifprofil) {
            modifierProfil();
            new Profil(disp, utilisateur);
        }
         if(c==cmdsupuser)      {   
           new Authentification(disp);
                 supprimeruser();}
       form.addCommand(cmdd);
        if (c == cmdd) {
         //   new ConsulterOffre(display, utilisateur);
        
        
//
        }

    }
public void supprimeruser() {
           
            Thread th1 = new Thread() {
                public void run() {
                  UserHandler userhand = new UserHandler();
                   int idc= utilisateur.getId();
                   
                    try {
                        String url = "http://localhost/parsing2/supprimerUser.php";
                        StringBuffer sb = new StringBuffer();
                        System.out.println(utilisateur.toString());
                        String urlconn = url + "?id=" +idc;
                        HttpConnection hc1 = (HttpConnection) Connector.open(urlconn);
                        DataInputStream dis = new DataInputStream(hc1.openDataInputStream());

                        
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            };
            th1.start();

        }
    private String showUtilisateur() {
        String res = "";

        sb.append(" *Nom: ");
        sb.append(utilisateur.getUsername());
        sb.append("\n  ");
        sb.append("*email: ");
        sb.append(utilisateur.getEmail());
        sb.append("\n  ");
        sb.append("*mot de passe: ");
         sb.append(utilisateur.getPassword());
        sb.append("\n  ");
        
        try {
            img = Image.createImage("/images/profil.jpg");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        form.deleteAll();

        sb.append("\n");


        res = sb.toString();
        sb = new StringBuffer("");
        return res;
    }

    private void updateProfil(User utilisateur) {

        newnom.setString(utilisateur.getUsername());
        newmail.setString(utilisateur.getEmail());
        newpassword.setString(utilisateur.getPassword());
       

        modif.append(newnom);
        modif.append(newmail);
        modif.append(newpassword);
        disp.setCurrent(modif);
    }

    private void modifierProfil() {
        new Thread() {
            public void run() {
                try {
                    String nom = newnom.getString();
                    String password = newpassword.getString();
                    String email = newmail.getString();
                    int id = utilisateur.getId();
                    utilisateur.setEmail(email);
                    utilisateur.setPassword(password);
                    utilisateur.setUsername(nom);
                                       System.out.println(id);
    System.out.println(id);
                    String url = "http://localhost/parsing2/modifierUtilisateur.php";
                    HttpConnection hc = (HttpConnection) Connector.open(url + "?username=" + nom +  "&password=" + password + "&email=" 
                      + email  + "&id=" + id);
                    DataInputStream dis = new DataInputStream(hc.openDataInputStream());
                    while ((ch = dis.read()) != -1) {
                        sb.append((char) ch);
                    }

//                    alerta.setTitle("Excellent travail");
//                    alerta.setString("Votre Profil a été modifié avec succès\n");
//                    alerta.setType(AlertType.INFO);
//                    alerta.setTimeout(3000);
//                    disp.setCurrent(alerta);


                    String result = sb.toString().trim();
                    String resultat = result.substring(result.length() - 18);
                    if (resultat.equalsIgnoreCase("successfully added")) {
                        disp.flashBacklight(3);
                        Alert al = new Alert("ok", "modification effectuée", null, AlertType.INFO);
                         al.setTimeout(2000);
                        disp.setCurrent(al);
//                        form.deleteAll();
//                        form.append(showUtilisateur());
//                       // disp.setCurrent(form);
//                        //Thread.sleep(1000);
                        new Profil(disp,utilisateur) ;
                        
                    } else {
                        Alert al = new Alert("desolé");
                        al.setTitle("Erreur");
                        System.out.println(sb.toString().trim());
                        al.setString("désolé, modification échouée " + sb.toString().trim());
                        al.setType(AlertType.ERROR);
                        al.setTimeout(2000);
                        sb = new StringBuffer("");
                        disp.setCurrent(al);
                        //Thread.sleep(1000);
                    }



                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }.start();
    }
}
