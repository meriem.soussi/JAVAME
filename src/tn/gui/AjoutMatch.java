/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.gui;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Iserv
 */
public class AjoutMatch extends Form implements CommandListener {
 

    TextField tType= new TextField("Type", "", 10, TextField.ANY);
    TextField tlieux = new TextField("lieux", "", 10, TextField.ANY);
    TextField tDateDebut = new TextField("DateDebut", "", 10, TextField.ANY);

    TextField heurefin = new TextField("heure fin", "", 10, TextField.ANY);
    Command cmdNext = new Command("Add", Command.OK, 0);
    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb;

    String url = "http://localhost/parsing2/ajoutCatalogue.php?";

    public AjoutCatalogue() {
        super("Ajout");
        append(tType);
        append(tDescription);
       append(tImage);

        addCommand(cmdNext);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdNext) {
            try {
                String type, description,image;
                type = tType.getString();
                description = tDescription.getString();
                image = tImage.getString();

                hc = (HttpConnection) Connector.
                        open(url + "type=" + type + "&lieux=" + description+"&datedebut="+datedebut);
                dis = hc.openDataInputStream();

                int ascii;
                sb = new StringBuffer();

                while ((ascii = dis.read()) != -1) {

                    sb.append((char) ascii);

                }

                if (sb.toString().equals("successfully added")) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                   // Midlet.mMidlet.disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                   // Midlet.mMidlet.disp.setCurrent(a);
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }

}

    
}
