/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.gui;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.TextField;
import javax.microedition.lcdui.Ticker;

public class Inscription implements CommandListener {

    private Form formulaireInscription = new Form("Inscription");
    private TextField username = new TextField("username  ", null, 20, TextField.ANY);
    private TextField email = new TextField("email", null, 40, TextField.EMAILADDR);
    private TextField mdp = new TextField("Mot de Passe", null, 30, TextField.PASSWORD);
    private TextField confirmMdp = new TextField("Confirmation mot de Passe", null, 30, TextField.PASSWORD);
    private String[] typeItems = {"Client","ResponsableAntiDopage","Medecin"};
    private ChoiceGroup roles = new ChoiceGroup("select one type", ChoiceGroup.POPUP, typeItems, null);
    private TextField imageUrl = new TextField("ImageUrl", null, 50, TextField.ANY);

    private Image photo;
    private ImageItem imageItem;
    
    private Command cmdValider = new Command("Valider", Command.SCREEN, 0);
    private Command cmdSelectImg = new Command("Image", Command.SCREEN, 0);
    private Command cmdBack = new Command("Retour", Command.SCREEN, 0);
    
    private Ticker tk = new Ticker("Inscrivez Vous");
    private Display display;
    

    Inscription(Display display) {
        this.display = display;
        try {
            photo = Image.createImage("/images/bal.jpg");
            imageItem = new ImageItem("", photo, ImageItem.LAYOUT_LEFT | ImageItem.LAYOUT_TOP, "photo");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        formulaireInscription.setTicker(tk);
        formulaireInscription.append(imageItem);
        formulaireInscription.append(username);
        formulaireInscription.append(mdp);
        formulaireInscription.append(confirmMdp);
        formulaireInscription.append(email);
        formulaireInscription.append(roles);
          formulaireInscription.append(imageUrl);

        formulaireInscription.addCommand(cmdValider);
        formulaireInscription.addCommand(cmdBack);
        formulaireInscription.addCommand(cmdSelectImg);
        formulaireInscription.setCommandListener(this);
        display.setCurrent(formulaireInscription);
    }

    
    public void commandAction(Command c, Displayable d) {
        if(c==cmdValider){
            inscriptionHttp();
        }else if(c==cmdSelectImg){
            selectImage();
        }else if (c==cmdBack){
            new Authentification(display);
        }
        
    }

    public void inscriptionHttp() {
        Thread th = new Thread() {
            public void run() {
                try { 
                    String url = "http://localhost/parsing2/ajoututilisateurs.php";
                    HttpConnection hc;
                    DataInputStream dis;
                    StringBuffer sb = new StringBuffer();
                    String strMotDePasse = mdp.getString().trim();
                    System.out.println(strMotDePasse);
                    String strusername = username.getString().trim();
                   String strusername_canonical	= username.getString().trim();

                    String stremail = email.getString().trim();
                    String stremail_canonical = email.getString().trim();
                    
                   String strpassword =mdp.getString().trim();
                    String strroles =roles.getString(roles.getSelectedIndex());
                    String strenabled ="1";
                    String strlocked = strenabled;
                    String strsalt ="5";
                    String strlast_login = "5";
                    String strexpired = "56";
                    String strexpires_at = "6";
                    String strconfirmation_token = "6";
                    String strpassword_requested_at = "565";
                    String strcredentials_expired = "56";
                    String strcredentials_expire_at = "56";
                    String imageUrlString = imageUrl.getString();
                    // System.out.println("alooooooooooooooooo");
                
                    int ch;
                    Alert alerta = new Alert(null, null, null, AlertType.ERROR);
                    hc = (HttpConnection) Connector.open(url + 
                            "?Usernamecanonical=" + strusername
                            +"&username=" + strusername
                            +"&email=" + stremail
                            +"&roles=" + strroles
                            + "&emailcanonical=" + stremail 
                            + "&locked="+strlocked
                            + "&password="+strpassword
                            + "&salt="+strsalt
                            + "&lastlogin="+strlast_login
                            + "&expired="+strexpired
                            + "&expires_at="+strexpires_at
                            + "&confirmationtoken="+strconfirmation_token
                            + "&passwordrequested_at="+strpassword_requested_at
                            + "&credentialsexpired="+strcredentials_expired
                            + "&credentialsexpire_at="+strcredentials_expire_at
                            + "&image=" + imageUrlString
                            + "&enabled=" + strenabled );
                   
                    // ?nom=a+prenom=a+pseudo=a+email=a+passe=a+adresse=a+telephone=123+role=client
                    dis = new DataInputStream(hc.openDataInputStream());
                    while ((ch = dis.read()) != -1) {
                        sb.append((char) ch);
                    }
                    if ("OK".equalsIgnoreCase(sb.toString().trim())) {
                       
                        mdp.setString("");
                        confirmMdp.setString("");
                        email.setString("");
                        username.setString("");
                        alerta.setTitle("Excellent travail");
                        alerta.setString("Abonne ajouté avec succes\najoutez un autre si vous voulez");
                        alerta.setTimeout(4000);
                        alerta.setType(AlertType.INFO);

                        display.setCurrent(alerta);
                    } else {
                        alerta.setTitle("Erreur");
                        alerta.setString("désolé, ajout échoué");
                        alerta.setType(AlertType.ERROR);
                        alerta.setTimeout(2000);
                        display.setCurrent(alerta);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        };
        th.start();
    }

    public void selectImage() {
        
    }
}
