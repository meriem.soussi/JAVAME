package tn.gui;

import handlers.CatalogueHandler;
import handlers.UserHandler;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.Ticker;
import tn.entities.Catalogue;
import tn.entities.User;

public class ConsulterDetailCatalogue implements CommandListener {

    private Form formDetailCatalogue = new Form("Detail de l'catalogues");
    private Command cmdBackDetailCatalogue = new Command("retour", Command.BACK, 0);
    private Command cmdsupcatalogue = new Command("Supprimer Catalogue ", Command.SCREEN, 0);

    Command cmvid = new Command("map", Command.SCREEN, 2);
    private Command comment = new Command("commenter l'catalogue", Command.SCREEN, 0);
    private Command displaycom = new Command("afficher les commentaires", Command.SCREEN, 0);
    private Display display;
    private Ticker tk;
    private List lst = new List("Catalogues", List.IMPLICIT);
    private Catalogue catalogue;
    Image image;
    private User utilisateur;

    ConsulterDetailCatalogue(Display d, Catalogue exp, User utilisateur) {
        this.display = d;
        this.catalogue = exp;
        this.utilisateur = utilisateur;
        formDetailCatalogue.setCommandListener(this);
        tk = new Ticker("les details de l'catalogue ");
        formDetailCatalogue.setTicker(tk);
        Thread th = new Thread() {
            public void run() {

                image = getimg(catalogue.getImage());
                display.setCurrent(new CanvasDetails());
            }
        };
        th.start();
    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdBackDetailCatalogue) {
            new ConsulterCatalogue(display, utilisateur);
        }

        if (c == cmdsupcatalogue) {
           new ConsulterCatalogue(display, utilisateur);
            supprimerCatalogue();
     

        }
    }
    public void supprimerCatalogue() {
           
            Thread th1 = new Thread() {
                public void run() {
                  CatalogueHandler cathand = new CatalogueHandler();
                   int idc= catalogue.getIdCatalogue();
                   
                    try {
                        String url = "http://localhost/parsing2/supprimerCatalogue.php";
                        StringBuffer sb = new StringBuffer();
                        System.out.println(utilisateur.toString());
                        String urlconn = url + "?idCatalogue=" +idc;
                        HttpConnection hc1 = (HttpConnection) Connector.open(urlconn);
                        DataInputStream dis = new DataInputStream(hc1.openDataInputStream());

                        
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            };
            th1.start();

        }

    class CanvasDetails extends Canvas implements CommandListener {

        int w = getWidth();
        int h = getHeight();
        private final int IMAGE_AND_TEXT_DISTANCE = 10;

        public CanvasDetails() {

            addCommand(displaycom);
            addCommand(comment);
            addCommand(cmvid);
            addCommand(cmdsupcatalogue);

            addCommand(cmdBackDetailCatalogue);
            setCommandListener(this);
            display.setCurrent(formDetailCatalogue);
        }

        protected void paint(Graphics g) {
            g.setColor(255, 255, 255);
            g.fillRect(0, 0, w, h);
            g.setColor(0, 0, 0);
            // g.drawString("" + catalogue.getimg(), w / 7, h / 8, 0);
            g.drawImage(image, w / 2 - image.getWidth() / 2, h / 10, 0);

            String catalogueTitle = catalogue.getType() + " " + catalogue.getDescription();

            Font fontTitle = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM);

            g.setFont(fontTitle);

            Vector catalogueTitleLines = wrapToLines(catalogueTitle, fontTitle, 100);

            int y = h / 100 + image.getHeight() + IMAGE_AND_TEXT_DISTANCE;
            int linespacing = 4;
            for (int i = 0; i < catalogueTitleLines.size(); i++) {
                g.drawString((String) catalogueTitleLines.elementAt(i), w / 2 - 50, y, 0);
                //y+=(i!=catalogueTitleLines.size()-1)?(fontTitle.getHeight()+linespacing):0;
                y += fontTitle.getHeight() + linespacing;
            }

            String catalogueDesc = catalogue.getDescription() + " " + catalogue.getType() + " à : \n"
                    + catalogue.getNom() + " " + catalogue.getDescription();
            Font fontDesc = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
            g.setFont(fontDesc);
            Vector catalogueStringLines = wrapToLines(catalogueDesc, fontDesc, 220);

            for (int i = 0; i < catalogueStringLines.size(); i++) {
                g.drawString((String) catalogueStringLines.elementAt(i), 10, y, 0);
                y += (i != catalogueStringLines.size() - 1) ? (fontDesc.getHeight() + linespacing) : 0;
            }
            //g.drawString(catalogueString,0, h /2, 0);
        }

        public void commandAction(Command c, Displayable d) {

            if (c == cmdBackDetailCatalogue) {
                new ConsulterCatalogue(display, utilisateur);
            }
//            if (c == comment) {
//                new AddComment(catalogue, utilisateur, display);
//            }
//             if (c == displaycom) {
//            new AfficherComs(display , catalogue ,  utilisateur);
//        }
        }
    }

    public Image getimg(String url) {
        DataInputStream is = null;

        Image img = null;
        try {
            HttpConnection c = (HttpConnection) Connector.open(url);
            int len = (int) c.getLength();
            if (len > 0) {
                is = c.openDataInputStream();
                byte[] data = new byte[len];
                is.readFully(data);
                img = img.createImage(data, 0, len);
                return img;
            }
            is.close();
            c.close();
            return img;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }

    public static Vector wrapToLines(String text, Font f, int maxWidthPerLine) {
        Vector lines = new Vector();
        boolean paragraphFormat = false;
        if (text == null) {
            return lines;
        }
        if (f.stringWidth(text) < maxWidthPerLine) {
            lines.addElement(text);
            return lines;
        } else {

            char chars[] = text.toCharArray();
            int len = chars.length;
            int count = 0;
            int charWidth = 0;
            int curLinePosStart = 0;
            while (count < len) {
                if ((charWidth += f.charWidth(chars[count])) > (maxWidthPerLine - 4) || count == len - 1) // wrap to next line
                {
                    if (count == len - 1) {
                        count++;
                    }
                    String line = new String(chars, curLinePosStart, count - curLinePosStart);
                    if (paragraphFormat) {
                        int lastSpacePosition = -1;
                        for (int j = line.length() - 1; j > 0; j--) {
                            if (line.substring(j, j) == " ") {
                                lastSpacePosition = j;
                            }
                        }
                        String l = new String(chars, curLinePosStart, (lastSpacePosition != -1) ? lastSpacePosition + 1 : (count == len - 1) ? count - curLinePosStart + 1 : count - curLinePosStart);
                        lines.addElement(l);
                        curLinePosStart = (lastSpacePosition != -1) ? lastSpacePosition + 1 : count;
                    } else {
                        lines.addElement(line);
                        curLinePosStart = count;
                    }
                    charWidth = 0;
                }
                count++;
            }
            return lines;

        }
    }
}
