package tn.gui;

import tn.entities.UTilisateur;


import handlers.UserHandler;
import java.io.DataInputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class ConsulterUser implements CommandListener {

    iPhoneCanvas selector;
    private User utilisateur;
    private Display display;
    private User[] users;
    String labels[] = {"", "", "", "", "", "", ""};
    Command cmdBackMenu = new Command("Retour", Command.BACK, 0);
    Command cmdAjouterUser = new Command("Ajouter", Command.OK, 0);
    Image[] img = new Image[5];

    public ConsulterUser(Display display, User utilisateur) {

        selector = new iPhoneCanvas("liste des users", List.IMPLICIT,labels,img);
        this.display = display;
        this.utilisateur = utilisateur;
        selector.addCommand(cmdBackMenu);
        //selector.addCommand(cmdAjouterUser);
        selector.setCommandListener(this);
        getUsersHttp();
        display.setCurrent(selector);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBackMenu) {
            new Acceuil(display, utilisateur);
        }
    /*if (c == cmdAjouterUser) {
          new Inscription(display, utilisateur);
        }*/
     /*  if (c == List.SELECT_COMMAND) {
            for(int j=0 ;j<users.length;j++)
            {System.out.println(users[j]);}
            System.out.println("list" + selector.getSelectedIndex() + " " + users.length);
            int a = (int) (selector.getSelectedIndex()+3) %users.length;
            if (a < 0) {
                a = users.length + a;
            }
            System.out.println("indice" + a);
          //  ConsulterDetailUser consulterDetailUser = new ConsulterDetailUser(display, users[a], utilisateur);
        }*/
    }

    private void getUsersHttp() {
        Thread th = new Thread() {
            public void run() {
                selector.deleteAll();
                users = null;
                try {
                    UserHandler userHandler = new UserHandler();
                    // get a parser object
                  
                    SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                    // get an InputStream from somewhere (could be HttpConnection, for example)
                    HttpConnection hc = (HttpConnection) Connector.open("http://localhost/parsing2/getXmlUtilisateur.php");//people.xml est un exemple
                    DataInputStream dis = new DataInputStream(hc.openDataInputStream());
                    System.out.println("0");
                    parser.parse(dis, userHandler);
                    System.out.println("1");
                    // display the result
                    
                    users = userHandler.getUser();
                    System.out.println(users.length);
                    img = new Image[users.length];
                 
                    if (users.length > 0) {  ;
                        for (int i = 0; i < users.length; i++) { 
                            System.out.println(users[i].getUsername());
                            labels[i] = users[i].getEmail()+" Nom: "+users[i].getRoles();
                           
                                img[i] = getImage(users[i].getImage());
                                  System.out.println("fer");
                               //Image im = Image.createImage("/images/thumb8.jpg");
                               //img[i] = im;
                                System.out.println(" label "+labels[i]);
                           
                                selector.append(labels[i], img[i]);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Exception:" + e.toString());
                }
                System.out.println(users.length);
                display.setCurrent(selector);
            }
        };
        th.start();
    }

    public Image getImage(String url) {
        DataInputStream is = null;

        Image img = null;
        try {
            HttpConnection c = (HttpConnection) Connector.open(url);
            int len = (int) c.getLength();
            if (len > 0) {
                is = c.openDataInputStream();
                byte[] data = new byte[len];
                is.readFully(data);
                img = Image.createImage(data, 0, len);
                return img;
            }
            is.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
        return null;
    }
}

