/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handlers;

import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import tn.entities.Match;

/**
 *
 * @author Iserv
 */
public class MatchHandler extends DefaultHandler{
    private Vector matchs;
    
    private Match currentMatch;
    
    private String id;
    private String lieux ;
    private String type ;
    private Date date_debut ;
    private Date heure_fin ;

    public Vector getMatchs() {
        return matchs;
    }

    public void setMatchs(Vector matchs) {
        this.matchs = matchs;
    }
    public MatchHandler() {
        matchs = new Vector();
    }
    
    public Match[] getTabMatchs() {
        Match[] matchTab = new Match[matchs.size()];
        matchs.copyInto(matchTab);
        return matchTab;
    }
    
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("match")) {
            currentMatch = new Match();
            currentMatch.setId(Integer.parseInt(attributes.getValue("idMatch")));
            currentMatch.setLieux(attributes.getValue("lieux"));
            currentMatch.setType(attributes.getValue("Type"));
            currentMatch.setDate_debut(parseDate(attributes.getValue("Date debut")));
          

        }else if (qName.equals("match")) {
            // we are no longer processing a <reg.../> tag
            matchs.addElement(currentMatch);
            currentMatch = null;
        } else if (qName.equals("id")) {
            id = "open";
        } else if (qName.equals("lieux")) {
            lieux = "open";
        } else if (qName.equals("type")) {
            type = "open";
        } else if (qName.equals("date debut")) {
            date_debut = "open";
        } else if (qName.equals("heure fin")) {
            heure_fin = "open";
        
        }  

    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("match")) {
            matchs.addElement(currentMatch);
            currentMatch = null;
        } else if (qName.equals("idMatch")) {
            id = "close";
        } else if (qName.equals("lieux")) {
            lieux = "close";
        
        } else if (qName.equals("type")) {
            type = "close";
           
            } else if (qName.equals("date debut")) {
            date_debut = "close";
        } else if (qName.equals("heure_fin")) {
            heure_fin = "close";
        } 
    }
    
}
