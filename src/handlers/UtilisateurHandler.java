package handlers;

import java.util.Vector;
import org.xml.sax.helpers.DefaultHandler;
import tn.entities.Utilisateur;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;



public class UtilisateurHandler extends DefaultHandler{
   
    private Vector users;
    String idTag = "close";
    String useTag = "close";
    String useCTag="close";
    String emaTag = "close";
    String emaCTag = "close";
    String enTag = "close";
    String lastLoginTag="close";
    String passTag = "close";
    String lockedTag="close";
    String salTag = "close";
    String expiredTag="close";
    String expiresAtTag="close";
    String confirmationTokenTag="close";
    String passwordRequestedAtTag="close";
    String rolesTag="close";
    String credentialsExpiredTag="close";
    String credentialsExpiredAtTag="close";
    
        String imageTag="close";
         private String longitudeTag;
    private String latitudeTag;

    public UtilisateurHandler() {
        users = new Vector();
    }

    public Utilisateur[] getUtilisateur() {
        Utilisateur[] userss = new Utilisateur[users.size()];
        users.copyInto(userss);
        return userss;
    }
    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Utilisateur currentUser;

    // XML EVENT PROCESSING METHODS (DEFINED BY DefaultHandler)
    // startElement is the opening part of the tag "<tagname...>"
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("Utilisateur")) {
            currentUser = new Utilisateur();
            //2ème methode pour parser les attributs
            currentUser.setId(Integer.parseInt(attributes.getValue("id")));
            currentUser.setUsername(attributes.getValue("username"));

          //  currentUser.setUsernameCanonical(attributes.getValue("usernameCanonical"));
            currentUser.setEmail(attributes.getValue("email"));
            
//         currentUser.setEnabled(Integer.parseInt(attributes.getValue("enabled")));
        //    currentUser.setLocked(Integer.parseInt(attributes.getValue("locked")));

        //    currentUser.setExpired(Integer.parseInt(attributes.getValue("expired")));
            currentUser.setRoles((attributes.getValue("roles")));
          //  currentUser.setCredentialsExpired(Integer.parseInt(attributes.getValue("credentials_expired")));
            //currentUser.setEmailCanonical(attributes.getValue("email_canonical"));
            currentUser.setPassword(attributes.getValue("password"));
                        currentUser.setImage(attributes.getValue("image"));

          //  currentUser.setSalt(attributes.getValue("salt"));
            /****/
//          currentUser.setAdresseLatitude(Double.parseDouble(attributes.getValue("adresseLatitude")));
      //  currentUser.setAdresseLongitude(Double.parseDouble(attributes.getValue("adresseLongitude")));
            
        } else if (qName.equals("id")) {
            idTag = "open";
        } else if (qName.equals("username")) {
            useTag = "open";
        }else if (qName.equals("email")) {
            useTag = "open";
        } else if (qName.equals("password")) {
            passTag = "open";
        }
           else if (qName.equals("roles")) {
            rolesTag= "open";
        }
        else if (qName.equals("image")) {
            imageTag= "open";
        }
        /* else if (qName.equals("adresseLatitude")) {
            latitudeTag= "open";
        }
         else if (qName.equals("adresseLongitude")) {
            longitudeTag= "open";
        }*/
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("user")) {
            // we are no longer processing a <reg.../> tag
            users.addElement(currentUser);
            currentUser = null;
       } else if (qName.equals("id")) {
            idTag = "open";
        } else if (qName.equals("username")) {
            useTag = "open";
        } else if (qName.equals("email")) {
            useTag = "open";
        
        }else if (qName.equals("password")) {
            passTag = "open";
        }else if (qName.equals("nom")) {
            latitudeTag = "open";
        }else if (qName.equals("adresseLongitude")) {
            longitudeTag = "open";
        }
    }
    // "characters" are the text between tags

    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        if (currentUser != null) {
            // don't forget to trim excess spaces from the ends of the string
            if (idTag.equals("open")) {
                String id = new String(ch, start, length).trim();
                currentUser.setId(Integer.parseInt(id));
            } 
                if (useTag.equals("open")) {
                String username = new String(ch, start, length).trim();
                currentUser.setUsername(username);
            } 
               
               
                    else
                    if (emaCTag.equals("open")) {
                String email = new String(ch, start, length).trim();
                currentUser.setEmailCanonical(email);
            }
                    else
                        
                    if (passTag.equals("open")) {
                String password = new String(ch, start, length).trim();
                currentUser.setPassword(password);
            }
                 else if (rolesTag.equals("open")) {
                String roles = new String(ch, start, length).trim();
                currentUser.setCredentialsExpired(Integer.parseInt(roles));
                    }
                 else if (imageTag.equals("open")) {
                String image = new String(ch, start, length).trim();
                currentUser.setCredentialsExpired(Integer.parseInt(image));
                    }
               
        }
    }
    
}
